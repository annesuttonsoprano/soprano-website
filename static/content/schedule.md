---
id: schedule
title: Schedule
---

# Upcoming Events:


## Opera Scenes <sub><sup>**- April 29th and 30th, 2021 | Royal Academy of Music**<sup><sub>

Anne sings the role of Sophie in Massenet’s Werther as part of the Royal Academy of Music Opera Scenes program. Conducted by Philip Sunderland and directed by Harry Fehr

## Guitar and Voice Concert <sub><sup>**- March 11th, 2021 | Royal Academy of Music**<sup><sub>

Anne performs a selection of songs with the Royal Academy of Music guitar faculty   

--- 

# Past Highlights:

## BWV 75: Die Elenden sollen essen <sub><sup>**- January 17th, 2021 (POSTPONED due to Covid-19) | Royal Academy of Music**<sup><sub>

Anne performs as the soprano soloist in Bach’s Cantata 75 as part of the Academy’s Bach the European concert series. Conducted by Eamonn Dougan.

## Dixit Dominus <sub><sup>**- November 29th, 2020 | Royal Academy of Music**<sup><sub>

Anne performs as the soprano soloist in Handel’s Dixit Dominus as part of the Academy’s Bach the European concert series. Conducted by Iain Ledingham.

## A Midsummer Night’s Dream <sub><sup>**- November 27th and 28th, 2020 | Royal Academy Opera**<sup><sub>

Anne performs as one of Tytania’s fairies in Royal Academy Opera’s innovative production of Benjamin Britten’s A Midsummer Night’s Dream

## Recital <sub><sup>**- February 4th, 2020 | North Carolina Historically Informed Performance Festival**<sup><sub>

Anne returns to North Carolina to sing in a lecture-recital created by Dr. Jeanne Fischer, “Madama Europa di Rossi: A Jewish soprano in 16th century Mantua.” Performed by Jeanne Fischer (soprano), Shafali Jalota (soprano), Anne Sutton (soprano), Jacqueline Nappi (harpsichord), and Brent Wissick (viola da gamba)

## Alcina <sub><sup>**- Summer 2020, cancelled due to Covid-19 | Chicago Summer Opera Festival**<sup><sub>

Anne fully prepared the role of Morgana, though was unfortunately unable to perform the role due to the Covid-19 pandemic

## Litanies de la Vierge <sub><sup>**- July 2018 | International Baroque Institute at Longy**<sup><sub>

Anne performs as a featured soloist in Charpenier’s stunning choral work following a week of intensive masterclasses in French Baroque repertoire with James Taylor.

## Monteverdi Scenes Concert <sub><sup>**- June 2018 | Queens College Baroque Opera Workshop**<sup><sub> 

Anne performs the role of Amore in Monteverdi’s L’incoronazione di Poppea after studying Baroque gesture with director Nell Snaidas.

## Magnificat <sub><sup>**- April 2018 | Carolina Choir**<sup><sub>

Anne performs as soprano soloist and chorus member in UNC Carolina Choir’s performance of Bach’s Magnificat with the Chapel Hill Chamber Orchestra. Conducted by Susan Klebanow.

## Messiah <sub><sup>**- December 2017 | Chapel Hill Newman Center**<sup><sub>

Anne performs as the soprano soloist in Handel’s holiday classic. Conducted by Daniel Cherrix.

## Cendrillon <sub><sup>**- November 2017 | UNC Opera**<sup><sub>

Anne makes her role debut as the Fairy Godmother, or La Fée, in Massenet’s Cendrillon. Stage directed by Marc Callahan and music directed by Qiao Zheng Goh.

## The Magic Flute <sub><sup>**- October 2016 | Opera Company of Middlebury**<sup><sub> 

Anne performs as the First Spirit in the Opera Company of Middlebury’s playful production of Mozart’s The Magic Flute. Conducted by Jeffrey Rink and directed by Doug Anderson

