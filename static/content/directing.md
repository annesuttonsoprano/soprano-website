---
id: directing
title: Directing
---

Anne Sutton has also worked internationally as a stage director. She most recently worked as an assistant director under Amanda Consol for the Maryland Opera Studio, bringing to life the world premiere of The Four Freedoms as part of their New Work Festival. Also at the University of Maryland, she directed Patience with OperaTerps, the university’s undergraduate opera program. She was the director of the world premiere of The Tomb of Beauty (Steven Crino) at Peabody Conservatory, though unfortunately the Covid-19 pandemic struck midway through the production process. Anne also assistant directed Alcina at the Halifax Summer Opera Festival in Nova Scotia under Erin Bardua. Her interest in directing began in her native Vermont, where for many summers she assistant directed shows with the Very Merry Theater Company, helping to create traveling children’s theater productions that were performed throughout the state.